<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MemberUpdateTest extends TestCase
{
    /**
     * Mailchimp Update list member API endpoint response test.
     *
     * @return void
     */
     public function testMemberUpdate()
     {
       $response = $this->json('POST', '/api/member/update/60a33cca5d', ['email_address' => 'asalsswfd33@gmail.com','status' => 'unsubscribed']);

       $response
          ->assertStatus(200)
          ->assertExactJson([
              'created' => true,
          ]);
     }
}
