<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DeleteListTest extends TestCase
{
    /**
     * Mailchimp delete a particular list API endpoint response test.
     *
     * @return void
     */
    public function testDeleteList()
    {
      $response = $this->call('POST', '/api/list/delete/1c6e51074d');

      $response
         ->assertStatus(200)
         ->assertExactJson([
             'created' => true,
         ]);
    }
}
