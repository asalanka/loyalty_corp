<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MemberCreateTest extends TestCase
{
    /**
     * Mailchimp create list member API endpoint response test.
     *
     * @return void
     */
    public function testMemberCreate()
    {
      $response = $this->json('POST', '/api/member/create/60a33cca5d', ['email_address' => 'asalsswfd33@gmail.com','status' => 'subscribed']);

      $response
         ->assertStatus(200)
         ->assertExactJson([
             'created' => true,
         ]);
    }
}
