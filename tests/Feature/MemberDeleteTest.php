<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MemberDeleteTest extends TestCase
{
    /**
     * Mailchimp delete list member API endpoint response test.
     *
     * @return void
     */
     public function testMemberDelete()
     {
       $response = $this->json('POST', '/api/member/delete/60a33cca5d', ['email_address' => 'asalankau+asa32@gmail.com']);

       $response
          ->assertStatus(200)
          ->assertExactJson([
              'created' => true,
          ]);
     }
}
