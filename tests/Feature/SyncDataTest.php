<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SyncDataTest extends TestCase
{
    /**
     * Mailchimp POST sync data endpoint response test.
     *
     * @return void
     */
    public function testSyncData()
    {
      $response = $this->call('POST', '/api/sync');

      $response
         ->assertStatus(200)
         ->assertExactJson([
             'created' => true,
         ]);
    }
}
