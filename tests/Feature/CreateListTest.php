<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CreateListTest extends TestCase
{
    /**
     * Mailchimp create a particular list API endpoint response test.
     *
     * @return void
     */
    public function testCreateList()
    {
      $response = $this->json('POST', '/api/list/create', ['name' => 'new list 01','email' => 'asalankau@yahoo.com.au','address1' => 'Jones Ct','address2' => 'text','city' => 'Mount Waverley','state' => 'VIC','zip' => '3149','country' => 'AUS','phone' => '22232322',
      'company' => 'None','permission_reminder' => 'some text','from_name' => 'Asanka','from_email' => 'asalankau@yahoo.com.au','subject' => 'Subject info','language' => 'en','email_type_option' => true]);

      $response
         ->assertStatus(200)
         ->assertExactJson([
             'created' => true,
         ]);
    }
}
