<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UpdateListTest extends TestCase
{
    /**
     * Mailchimp Update list API endpoint response test.
     *
     * @return void
     */
     public function testUpdateList()
     {
       $response = $this->json('POST', '/api/list/update/d3f28a6915', ['name' => 'Old List 232','email' => 'asalankau@yahoo.com.au','address1' => 'Jones Ct','address2' => 'text','city' => 'Mount Waverley','state' => 'VIC','zip' => '3149','country' => 'AUS','phone' => '22232322',
       'company' => 'None','permission_reminder' => 'some text','from_name' => 'Asanka','from_email' => 'asalankau@yahoo.com.au','subject' => 'Subject info updated','language' => 'en','email_type_option' => true]);

       $response
          ->assertStatus(200)
          ->assertExactJson([
              'created' => true,
          ]);
     }
}
