# Loyalty Corp MailChimp Application
---

## Installation
 created a new public repository on bitbucket for this particular project, following SSH and HTTPS information will be required to clone the repository in a new environment.

   git@bitbucket.org:asalanka/loyalty_corp.git

   https://asalanka@bitbucket.org/asalanka/loyalty_corp.git


## Storage

storage -> app -> public

'list_create.json' will store create list data temporary and override on each create list request.

'list_delete.json' will store each delete list information and override on each delete list request.

'list_update.json' will store new updated list information and override on each updated list request.

'list_member_create.json' will store information regarding to create new member for required list ID, before processing data will be stored in the json file temporary.

'list_member_update.json' will store information regarding to update a member for required list ID, before processing data will be stored in the json file temporary.

'list_member_delete.json' will store information regarding to delete a member for required list ID, before processing data will be stored in the json file temporary.

## API end points

'api/list/create' end point will result create a new list in Mailchimp account based on name given.

'api/list/delete/{id}' end point will delete a particular list based on its Id in a particular mailchimp account based on its API key.

'api/list/update/{id}' end point will update a particular list based on its Id in a particular mailchimp account based on its API key.

'api/member/create/{id}' end point will create new member to a particular list based on the passing id.

'api/member/update/{id}' end point will update an existing member to a particular list based on the passing id.

'api/member/delete/{id}' end point will delete an existing member from a particular list based on the passing id.

'api/sync' will sync all the stored requests when required to be pushed in to MailChimp API.


## Test cases for feature testing

tests -> Feature

CreateListTest => Validate the api end point with data for 'api/list/create end point' and validate the json response after creating a list.

DeleteListTest => Validate the api end point with data for 'api/list/delete/{id}' and validate the json response after deleting a list.

UpdateListTest => Validate the api end point with new data for 'api/list/update/{id}' and validate the json response after updating a list.

MemberCreateTest => Validate the api end point with new data for 'api/member/create/{id}' and validate the json response after creating a member in to a particular list.

MemberUpdateTest => Validate the api end point with new data for 'api/member/update/{id}' and validate the json response after updating a member in to a particular list.

MemberUpdateTest => Validate the api end point with new data for 'api/member/delete/{id}' and validate the json response after deleting a member in a particular list.

Running test cases individually ./vendor/bin/phpunit --filter MemberCreateTest
