<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\mailChimpSync;
use Storage;

class ApiController extends Controller
{
  use mailChimpSync;

  public function __construct()
  {

  }

    /**
   * Create a mailchimp list resource and write into a json file.
   *
   * @return \Illuminate\Http\Response
   */
  public function createList(Request $request)
  {
     //prepare data array
     $data = array();
     $name = $request->input('name');
     $email = $request->input('email');
     $address1 = $request->input('address1');
     $address2 = $request->input('address2');
     $city = $request->input('city');
     $state = $request->input('state');
     $zip = $request->input('zip');
     $country = $request->input('country');
     $phone = $request->input('phone');
     $company = $request->input('company');
     $permission_reminder = $request->input('permission_reminder');
     $from_name = $request->input('from_name');
     $from_email = $request->input('from_email');
     $subject = $request->input('subject');
     $language = $request->input('language');
     $email_type_option = $request->input('email_type_option');

     $data['lists']['create']['name'] = $name;
      $data['lists']['create']['email'] = $email;
     $data['lists']['create']['contact']['address1'] = $address1;
     $data['lists']['create']['contact']['address2'] = $address2;
     $data['lists']['create']['contact']['city'] = $city;
     $data['lists']['create']['contact']['state'] = $state;
     $data['lists']['create']['contact']['zip'] = $zip;
     $data['lists']['create']['contact']['country'] = $country;
     $data['lists']['create']['contact']['phone'] = $phone;
     $data['lists']['create']['contact']['company'] = $company;
     $data['lists']['create']['permission_reminder'] = $permission_reminder;
     $data['lists']['create']['campaign_defaults']['from_name'] = $from_name;
     $data['lists']['create']['campaign_defaults']['from_email'] = $from_email;
     $data['lists']['create']['campaign_defaults']['subject'] = $subject;
     $data['lists']['create']['campaign_defaults']['language'] = $language;
     $data['lists']['create']['email_type_option'] = $email_type_option;


     //format the data
     $formattedData = json_encode($data);

    //set the filename
     $filename = storage_path(env('JSON_STORAGE_CREATE'));

    //open or create the file
    if (!file_exists($filename) || !is_readable($filename))
            return false;

        $data = array();

        if (($handle = fopen($filename, "w+")) !== false)
        {
          // //write the data into the file
           fwrite($handle,$formattedData);

           //close the file
           fclose($handle);

        }

        return response(['created' => true]);

  }

  /**
   * Create a mailchimp list member create resource and write into a json file.
   *
   * @return \Illuminate\Http\Response
   */
  public function createMember($id){
    //global helper function request()
    $request = request();
    $email = $request->input('email_address');
    $status = $request->input('status');

    $data['lists']['id'] = $id;
    $data['lists']['create']['member']['email_address'] = $email;
    $data['lists']['create']['member']['status'] = $status;

    //format the data
    $formattedData = json_encode($data);

   //set the filename
    $filename = storage_path(env('JSON_STORAGE_MEMBER_CREATE'));

   //open or create the file
   if (!file_exists($filename) || !is_readable($filename))
           return false;

       $data = array();

       if (($handle = fopen($filename, "w+")) !== false)
       {
         // //write the data into the file
          fwrite($handle,$formattedData);

          //close the file
          fclose($handle);

       }

       return response(['created' => true]);
  }


  /**
   * Update an existing mailchimp list member resource and write into a json file.
   *
   * @return \Illuminate\Http\Response
   */
  public function updateMember($id){
    //global helper function request()
    $request = request();
    $email = $request->input('email_address');
    $status = $request->input('status');

    $data['lists']['id'] = $id;
    $data['lists']['update']['member']['email_address'] = $email;
    $data['lists']['update']['member']['status'] = $status;

    //format the data
    $formattedData = json_encode($data);

   //set the filename
    $filename = storage_path(env('JSON_STORAGE_MEMBER_UPDATE'));

   //open or create the file
   if (!file_exists($filename) || !is_readable($filename))
           return false;

       $data = array();

       if (($handle = fopen($filename, "w+")) !== false)
       {
         // //write the data into the file
          fwrite($handle,$formattedData);

          //close the file
          fclose($handle);

       }

       return response(['created' => true]);

  }

  /**
   * Delete an existing mailchimp list member resource and write into a json file.
   *
   * @return \Illuminate\Http\Response
   */
  public function deleteMember($id){
    //global helper function request()
    $request = request();
    $email = $request->input('email_address');

    $data['lists']['id'] = $id;
    $data['lists']['delete']['member']['email_address'] = $email;

    //format the data
    $formattedData = json_encode($data);

   //set the filename
    $filename = storage_path(env('JSON_STORAGE_MEMBER_DELETE'));

   //open or create the file
   if (!file_exists($filename) || !is_readable($filename))
           return false;

       $data = array();

       if (($handle = fopen($filename, "w+")) !== false)
       {
         // //write the data into the file
          fwrite($handle,$formattedData);

          //close the file
          fclose($handle);

       }

       return response(['created' => true]);

  }

  /**
   * Update an existing mailchimp list resource and write into a json file.
   *
   * @return \Illuminate\Http\Response
   */
  public function updateList($id)
  {
    //global helper function request()
    $request = request();
    //prepare the data
    $data = array();
    $name = $request->input('name');
    $email = $request->input('email');
    $address1 = $request->input('address1');
    $address2 = $request->input('address2');
    $city = $request->input('city');
    $state = $request->input('state');
    $zip = $request->input('zip');
    $country = $request->input('country');
    $phone = $request->input('phone');
    $company = $request->input('company');
    $permission_reminder = $request->input('permission_reminder');
    $from_name = $request->input('from_name');
    $from_email = $request->input('from_email');
    $subject = $request->input('subject');
    $language = $request->input('language');
    $email_type_option = $request->input('email_type_option');

    $data['lists']['id'] = $id;
    $data['lists']['update']['name'] = $name;
    $data['lists']['update']['email'] = $email;
    $data['lists']['update']['contact']['address1'] = $address1;
    $data['lists']['update']['contact']['address2'] = $address2;
    $data['lists']['update']['contact']['city'] = $city;
    $data['lists']['update']['contact']['state'] = $state;
    $data['lists']['update']['contact']['zip'] = $zip;
    $data['lists']['update']['contact']['country'] = $country;
    $data['lists']['update']['contact']['phone'] = $phone;
    $data['lists']['update']['contact']['company'] = $company;
    $data['lists']['update']['permission_reminder'] = $permission_reminder;
    $data['lists']['update']['campaign_defaults']['from_name'] = $from_name;
    $data['lists']['update']['campaign_defaults']['from_email'] = $from_email;
    $data['lists']['update']['campaign_defaults']['subject'] = $subject;
    $data['lists']['update']['campaign_defaults']['language'] = $language;
    $data['lists']['update']['email_type_option'] = $email_type_option;


    //format the data
    $formattedData = json_encode($data);

   //set the filename
    $filename = storage_path(env('JSON_STORAGE_UPDATE'));

   //open or create the file
   if (!file_exists($filename) || !is_readable($filename))
           return false;

       $data = array();

       if (($handle = fopen($filename, "w+")) !== false)
       {
         // //write the data into the file
          fwrite($handle,$formattedData);

          //close the file
          fclose($handle);

       }

       return response(['created' => true]);

  }

  /**
   * Delete an existing mailchimp list resource and write into a json file.
   *
   * @return \Illuminate\Http\Response
   */
  public function removeList($id)
  {
    //prepare the data
    $data = array();
    $data['lists']['id'] = $id;
    $data['lists']['delete'] = true;

    //format the data
    $formattedData = json_encode($data);

   //set the filename
    $filename = storage_path(env('JSON_STORAGE_DELETE'));

   //open or create the file
   if (!file_exists($filename) || !is_readable($filename))
           return false;

       $data = array();

       if (($handle = fopen($filename, "w+")) !== false)
       {
         // //write the data into the file
          fwrite($handle,$formattedData);

          //close the file
          fclose($handle);

       }

       return response(['created' => true]);
  }

  /**
   * Process pending json payload and intiate call to MailChimp API to sync pending data with Mailchimp.
   *
   * @return \Illuminate\Http\Response
   */
  public function syncData(Request $request){

      $data = array();

      // Collecting json payload contents from the file
      $data = json_decode(file_get_contents(storage_path(env('JSON_STORAGE_CREATE'))), true);

      // Validate the payload for list create
      if (array_key_exists("lists", $data)) {
          if(array_key_exists("create", $data['lists'])){
              // Sync payload with Mailchimp API
              $this->syncMailchimp($data['lists']['create'],'lists', 'POST');
          }
      }

      // Collecting json payload contents from the file
      $data = json_decode(file_get_contents(storage_path(env('JSON_STORAGE_UPDATE'))), true);

      // Validate the payload for list update
      if (array_key_exists("lists", $data)) {
          if(array_key_exists("update", $data['lists'])){
              $id = $data['lists']['id'];
              // Sync payload with Mailchimp API
              $this->syncMailchimp($data['lists']['update'],'lists/'.$id, 'PATCH');
          }
      }

      // Collecting json payload contents from the file
      $data = json_decode(file_get_contents(storage_path(env('JSON_STORAGE_DELETE'))), true);

      // Validate the payload for deleting a list
      if (array_key_exists("lists", $data)) {
          if(array_key_exists("delete", $data['lists'])){
              $id = $data['lists']['id'];
              // Sync payload with Mailchimp API
              $this->syncMailchimp($data['lists']['delete'],'lists/'.$id, 'DELETE');
          }
      }

      // Collecting json payload contents from the file
      $data = json_decode(file_get_contents(storage_path(env('JSON_STORAGE_MEMBER_CREATE'))), true);

      // Validate the payload for list member create
      if (array_key_exists("lists", $data)) {
          if(array_key_exists("create", $data['lists'])){
              $id = $data['lists']['id'];
              // Sync payload with Mailchimp API
              $this->syncMailchimp($data['lists']['create']['member'],'lists/'.$id.'/members', 'POST');
          }
      }

      // Collecting json payload contents from the file
      $data = json_decode(file_get_contents(storage_path(env('JSON_STORAGE_MEMBER_UPDATE'))), true);

      // Validate the payload for list member update
      if (array_key_exists("lists", $data)) {
          if(array_key_exists("update", $data['lists'])){
              $id = $data['lists']['id'];
              //Member Hash value based on member @email
              $subscriberHash = md5(strtolower($data['lists']['update']['member']['email_address']));
              // Sync payload with Mailchimp API
              $this->syncMailchimp($data['lists']['update']['member'],'lists/'.$id.'/members/'.$subscriberHash, 'PATCH');
          }
      }

       // Collecting json payload contents from the file
       $data = json_decode(file_get_contents(storage_path(env('JSON_STORAGE_MEMBER_DELETE'))), true);

       // Validate the payload for list member delete
       if (array_key_exists("lists", $data)) {
           if(array_key_exists("delete", $data['lists'])){
               $id = $data['lists']['id'];
               //Member Hash value based on member @email
               $subscriberHash = md5(strtolower($data['lists']['delete']['member']['email_address']));
               // Sync payload with Mailchimp API
               $this->syncMailchimp($data['lists']['delete']['member'],'lists/'.$id.'/members/'.$subscriberHash, 'DELETE');
           }
       }

      return response(['created' => true]);
  }

}
