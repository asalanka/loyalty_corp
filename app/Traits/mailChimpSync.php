<?php namespace App\Traits;

trait mailChimpSync
{
    function syncMailchimp($data, $action, $method) {
      $apiKey = env('MAILCHIMP_API_KEY');
      $listId = env('MAILCHIMP_LIST_ID');
      $apiUrl = env('API_URL');

      // Preparing MailChimp datacenter infiromation
      $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
      $json = json_encode($data);

      $username = env('USER_NAME');
      $password = $apiKey;
      // create curl resource
      $ch = curl_init();
      // set url

      curl_setopt($ch, CURLOPT_URL, $apiUrl.$action);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
      curl_setopt($ch, CURLOPT_TIMEOUT, 10);
      //return the transfer as a string
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");

      // $output contains the output string
      $output = curl_exec($ch);
      // close curl resource to free up system resources

      $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

      curl_close($ch);

      //Return false if http response is greater than 300
      return ($httpcode >= 200 && $httpcode < 300) ? $data : false;

  }
}
