<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('list/create',"ApiController@createList");
Route::post('list/update/{id}',"ApiController@updateList");
Route::post('list/delete/{id}',"ApiController@removeList");
Route::post('member/create/{id}',"ApiController@createMember");
Route::post('member/update/{id}',"ApiController@updateMember");
Route::post('member/delete/{id}',"ApiController@deleteMember");
Route::post('sync',"ApiController@syncData");
